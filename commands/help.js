const PACKAGE = require('../package.json');

module.exports = {
    name: 'help',
    aliases: ["pomoc", "h"],
    description: 'Help command',
    usage: ["commands", "aliases"],
    execute(message, args) {
        if (args[0] == null) {
            const infoEmbed = {
                title: PACKAGE.name + ' Command List',
                // color: 0x709A46,
                fields: [
                    {
                        name: "🦠 Coronavirus",
                        value: "```\n!help corona```",
                        inline: true
                    },
                    {
                        name: "🔞 Fun",
                        value: "```\n!help nsfw```",
                        inline: true
                    },
                    {
                        name: "🔞 NSFW",
                        value: "```\n!help nsfw```",
                        inline: true
                    },
                    {
                        name: "🔞NSFW",
                        value: "```\n!help nsfw```",
                        inline: true
                    }
                ],
                timestamp: new Date(),
                footer: {
                    icon_url: "https://cdn.discordapp.com/avatars/692755389967761448/6abbf78d150ddcfd337738139c7554d2.png",
                    text: "Made with ♥ by RaccoonCodes"
                }
            }


            message.channel.send({ embed: infoEmbed });
        }
        else {
            if (args[0] === 'nsfw') {

                const helpnsfwEmbed = {
                    title: 'NSFW Command list help',
                    color: 0xd7141a,
                    fields: [
                        {
                            name: "🍒 Boobs",
                            value: "```\n!nsfw boobs```",
                            inline: true
                        },
                        {
                            name: "🍑 Booty",
                            value: "```\n!nsfw booty```",
                            inline: true
                        },
                        {
                            name: "🍊 Hentai",
                            value: "```\n!nsfw hentai```",
                            inline: true
                        }
                    ],
                    timestamp: new Date(),
                    footer: {
                        icon_url: "https://cdn.discordapp.com/avatars/692755389967761448/6abbf78d150ddcfd337738139c7554d2.png",
                        text: "Made with ♥ by RaccoonCodes"
                    }
                }


                message.channel.send({ embed: helpnsfwEmbed });

            } else if (args[0] === 'fun') {
                const helpfunEmbed = {
                    title: 'Fun Command list help',
                    color: 0xd7141a,
                    fields: [
                        {
                            name: "🥴 Cats",
                            value: "```\n!cats```",
                            inline: true
                        },
                        {
                            name: "🔞 Ducks ",
                            value: "```\n!duck```",
                            inline: true
                        },
                        {
                            name: "🔞NSFW",
                            value: "```\n!help nsfw```",
                            inline: true
                        }
                    ],
                    timestamp: new Date(),
                    footer: {
                        icon_url: "https://cdn.discordapp.com/avatars/692755389967761448/6abbf78d150ddcfd337738139c7554d2.png",
                        text: "Made with ♥ by RaccoonCodes"
                    }



                }
                message.channel.send({ embed: helpfunEmbed });

            }


        }
    }
}
