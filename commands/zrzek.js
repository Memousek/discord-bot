var RedditImagePost = require('../models/ImagePost/src/RedditImagePost');
var OwnImagePost = require("../models/ImagePost/src/OwnImagePost");
var ImagePost = require('../models/ImagePost/ImagePost');
var Help = require('../models/Help');
var Error = require('../models/Error');

module.exports = {
    name: 'z',
    aliases: ['zrzek', 'debilek'],
    async execute(message, args) {
        var embed;
        if (args[0] == "help" || args[0] == null) {
            embed = { embed: this.getHelp() };
        } else {
            var zrzek = global.assets.zrzek_categories;
            var types = {
              reddit: RedditImagePost,
              own: OwnImagePost,
            };

            if (zrzek[args[0]] != null && types[zrzek[args[0]].ImagePost_type] != null) {
                var curr = zrzek[args[0]];
                console.log(types[curr.ImagePost_type]);
                embed = await new ImagePost(new types[curr.ImagePost_type](...curr.params), "cs").get();
            } else {
                embed = Error.get(global.helpers.langs.get("cs", "command.unknown_command"));
            }
        }
        console.log(embed);
        if (!message.channel.nsfw) { message.channel.send(Error.get(global.helpers.langs.get("cs", "nsfw.channel")) ); return; }
        else message.channel.send(embed);
        return;

    },
    getHelp() {
        var zrzek = global.assets.zrzek_categories;
        var keys = Object.keys(zrzek);
        return Help.get("Zrzek", keys.map(x => {
            return {
                name: zrzek[x].emoji + " " + zrzek[x].name,
                command: "!z " + x
            }
        }))
    }
}
