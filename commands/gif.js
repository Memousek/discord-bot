var RedditImagePost = require('../models/ImagePost/src/RedditImagePost');
var ImagePost = require('../models/ImagePost/ImagePost');

module.exports = {
	name: 'gif',
	description: 'gifz',
	async execute(message, args) {
        var embed = await new ImagePost(new RedditImagePost("gifs"),"cs").get();

        message.channel.send(embed);
	},
};
