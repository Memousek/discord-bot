var RedditImagePost = require('../models/ImagePost/src/RedditImagePost');
var OwnImagePost = require("../models/ImagePost/src/OwnImagePost");
var ImagePost = require('../models/ImagePost/ImagePost');
var Help = require('../models/Help');
var Error = require('../models/Error');

module.exports = {
    name: 'nsfw',
    aliases: ["nsfw", "h"],
    description: 'Help command',
    usage: ["commands", "aliases"],
    async execute(message, args) {
        var embed;
        if (args[0] == "help" || args[0] == null) {
            embed = { embed: this.getHelp() };
        } else {
            var nsfw = global.assets.nsfw_categories;
            var types = {
              reddit: RedditImagePost,
              own: OwnImagePost,
            };

            if (nsfw[args[0]] != null && types[nsfw[args[0]].ImagePost_type] != null) {
                var curr = nsfw[args[0]];
                console.log(types[curr.ImagePost_type]);
                embed = await new ImagePost(new types[curr.ImagePost_type](...curr.params), "cs").get();
            } else {
                embed = Error.get(global.helpers.langs.get("cs", "command.unknown_command"));
            }
        }
        console.log(embed);
        if (!message.channel.nsfw) { message.channel.send(Error.get(global.helpers.langs.get("cs", "nsfw.channel")) ); return; }
        else message.channel.send(embed);
        return;
        // Boob NSFW Category
        /*
        switch (args[0]) {
            case 'boobs':

                break;
        }

        if (args[0] == 'boobs') {
            new ImagePost().get("boobs", message.channel);
        }

        // Booty NSFW Category

        if (args[0] === 'booty') {
            new ImagePost().get("booty", message.channel);
        }

        // Hentai NSFW Category

        if (args[0] === 'hentai') {
            new ImagePost().get("hentai", message.channel);
        }
        */
    },
    getHelp() {
        var nsfw = global.assets.nsfw_categories;
        var keys = Object.keys(nsfw);
        return Help.get("NSFW", keys.map(x => {
            return {
                name: nsfw[x].emoji + " " + nsfw[x].name,
                command: "!nsfw " + x
            }
        }))
    }
}
