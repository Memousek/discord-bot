const Discord = require('discord.js');
const Help = require('../models/Help');
var Error = require('../models/Error');

module.exports = {
	name: 'corona',
	description: 'mímz',
	async execute(storage,message, args) {
        var embed;
        switch(args[0]){
            case 'help':
        }

        var country = storage.cm.getCountryData(args[0]).catch(e=>{});

        await country
            .then(data=>{
                embed = {
                    embed: new Discord.MessageEmbed()
                        .setColor('#D7141A')
                        .setAuthor(country.Country + ' - Statistiky', 'https://www.countryflags.io/' + (args[0] != null ? args[0] : 'cz') + '/shiny/64.png', 'https://covid19api.com/')
                        .setThumbnail('https://images.discordapp.net/avatars/644977600057573389/b87fc36cb05fb1a9c21968b370b75d7f.png?size=512')
                        .addFields(
                            {name: 'Potvrzené případy', value: data.confirmed},
                            {name: 'Úmrtí', value: data.deaths, inline: true},
                            {name: 'Počet zotavených', value: data.recovered, inline: true},
                        )
                        .setTimestamp(data.updated_at)
                        .setFooter('© RaccoonCodes')
                }
            })
            .catch(e=>{
                console.log(e);
                embed = Error.get("Nelze načíst!");
            });

        message.channel.send(embed);
    },
    getHelp(){
	    return Help.get("corona",[
            {
                name:"Data pro ČR",
                command:"!corona"
            },
            {
                name:"Data pro stát",
                command:"!corona <značka státu>"
            }
        ])
    }
}
