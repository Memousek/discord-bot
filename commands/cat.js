var RedditImagePost = require('../models/ImagePost/src/RedditImagePost');
var ImagePost = require('../models/ImagePost/ImagePost');

module.exports = {
    name: 'cat',
	aliases: ['cats'],
	usage: '[command name]',
	cooldown: 5,
    description: 'Lovely Cat',
	async execute(message, args) {
        var embed = await new ImagePost(new RedditImagePost("cat"),"cs").get();

        message.channel.send(embed);
	},
};
