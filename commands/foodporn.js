var RedditImagePost = require('../models/ImagePost/src/RedditImagePost');
var ImagePost = require('../models/ImagePost/ImagePost');

module.exports = {
    name: 'foodporn',
	aliases: ['food'],
	usage: '[command name]',
	cooldown: 5,
    description: 'Lovely foodporn',
	async execute(message, args) {
        var embed = await new ImagePost(new RedditImagePost("foodporn"),"cs").get();

        message.channel.send(embed);
	},
};
