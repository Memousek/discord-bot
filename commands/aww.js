var RedditImagePost = require('../models/ImagePost/src/RedditImagePost');
var ImagePost = require('../models/ImagePost/ImagePost');

module.exports = {
    name: 'aww',
	aliases: ['aww'],
	usage: '[command name]',
	cooldown: 5,
    description: 'Lovely aww',
	async execute(message, args) {
        var embed = await new ImagePost(new RedditImagePost("aww"),"cs").get();

        message.channel.send(embed);
	},
};
