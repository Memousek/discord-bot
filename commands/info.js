const PACKAGE = require('../package.json');
module.exports = {
	name: 'info',
	description: 'Informace o serveru',
	execute(message, args) {
		if (args[1] == null) {
                const infoEmbed = {
                    title: 'Info about Bot',
                    color: 0x709A46,
                    fields: [
                        {
                            name: "Name",
                            value: PACKAGE.name,
                        },
                        {
                            name: "Version",
                            value: PACKAGE.version,
                        },
                        {
                            name: "Author",
                            value: PACKAGE.author,
                        },
                    ],
                    image: {
                        url: 'https://media.altphotos.com/cache/images/2017/07/14/08/752/baby-raccoon-grass.jpg'
                    },
                    timestamp: new Date(),
                    footer: {
                      icon_url: "https://cdn.discordapp.com/avatars/692755389967761448/6abbf78d150ddcfd337738139c7554d2.png",
                      text: "Made with ♥ by RaccoonCodes"
                    }
                }

                message.channel.send({ embed: infoEmbed });
            } else {

                if (args[1] == 'version') {
                    message.channel.send(PACKAGE.name + ' je ve verzi ' + PACKAGE.version);
                } else {
                    message.channel.send('Neplatný argument')
                }
            }
	},
};