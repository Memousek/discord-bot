const fs = require('fs');
const Discord = require('discord.js');
const { PREFIX, corona } = require('./config.json');
const PACKAGE = require('./package.json');
const token = process.env.token
const request = require('request');
const cron = require("node-cron");
const CoronaManager = require('./models/CoronaManager');
var Langs = require('./models/global/Langs');


const bot = new Discord.Client();
bot.commands = new Discord.Collection();


global.helpers = {
    langs:Langs
};

global.assets = {
    nsfw_categories:require('./config/nsfw.json'),
    zrzek_categories: require('./config/zrzek.json'),
}



const storage = {
    cm: new CoronaManager()
};

//storage.cm.setCountryDataInside("cz");


const commandFiles = fs.readdirSync('./commands').filter(file => file.endsWith('.js'));
console.log(commandFiles);
for (const file of commandFiles) {
    const command = require(`./commands/${file}`);
    bot.commands.set(command.name, command);
}


bot.on('ready', () => {
    console.log('Bot is turned on. Current version is: ' + PACKAGE.version);
    // load();

}, 1000);
;

/* ------------------------- Příkazy ---------------------------------- */

bot.on('message', async (message) => {
    if (!message.content.startsWith(PREFIX) || message.author.bot) return;

    const args = message.content.slice(PREFIX.length).split(/ +/);
    const command = args.shift().toLowerCase();

    switch (command) {
        case 'help':
            bot.commands.get("help").execute(message, args);
            break;
        case 'cat':
            bot.commands.get("cat").execute(message, args);
            break;
		case 'foodporn':
            bot.commands.get("foodporn").execute(message, args);
            break;
		case 'kencr':
            bot.commands.get("kencr").execute(message, args);
            break;
		case 'lenny':
            bot.commands.get("lenny").execute(message, args);
            break;
        case 'aww':
            bot.commands.get("aww").execute(message, args);
            break;
        case 'info':
            bot.commands.get("info").execute(message, args);
            break;
            // Zrzkova hrací karta
        case 'z':
            bot.commands.get("z").execute(message, args, storage.zrzek);
            break;
        case 'nsfw':
            bot.commands.get("nsfw").execute(message, args, storage.nsfw);
            break;
        case 'gif':
            bot.commands.get("gif").execute(message, args);
            break;
        case 'corona':
            bot.commands.get("corona").execute(storage, message, args);
            break;
    }

});

/* ----------------------------------------------------------- */

/* cron.schedule(corona.cron, function () {
    load();
});

 load(); */

/* ----------------------------------------------------------- */

/* function load(){
    storage.CoronaManager.
 } */


// 0 8,12,16,20 * * *
bot.login(token);

//TODO: Dodělání CoronaManageru, ukládání dat (zatím soubor, v budoucnu možná Mongo.db - done
//TODO: Vypsání do nastavených channelů pro coronu jen v případech, kdy se změní data. Př: každejch 30 minut proběhne API dotaz a pokud se výsledek změní, tak rozešli
//TODO: Možná načítání nsfw a kategorií a jejich zdrojů z JSON souboru? idk, bylo by to super, aber pomalý (pokud bychom tedy neudělali nějakej cache systém [takže celkem easy])
//TODO: Možnost načtení random příspěvku ze subredditu (user-input) nebo přímo načíst příspěvek
//TODO: Změna jazyka
//TODO: Možnost blokování kategorií v NSFW

