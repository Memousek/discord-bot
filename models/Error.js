class Error {
    static get(error){
        return {
            embed: {
                title: error,
                color: 0xff0000,
            }
        }
    }
}

module.exports = Error;
