class Help {
    static get(name,commands){

        var embed = {
            title: `${global.helpers.langs.get('cs','command.help.title')} `+"```"+`${name}`+"```",
            color: 0xd7141a,
            fields: [],
            timestamp: new Date(),
            footer: {
                icon_url: "https://cdn.discordapp.com/avatars/692755389967761448/6abbf78d150ddcfd337738139c7554d2.png",
                text: "Made with ♥ by RaccoonCodes"
            }
        };
        commands.forEach((x)=>{
            embed.fields.push({
                name: x.name,
                value: "```\n"+x.command+"```",
                inline: true
            });
        });
        return embed;
    }
}

module.exports = Help;
