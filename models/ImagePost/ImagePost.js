class ImagePost {

    type;
    lang;

    constructor(type, lang) {
        this.type = type;
        this.lang = lang;
    }

    async get() {
        var temp;
        const result = await this.type.run()
            .then((data) => {
                data.color = 0x709A46;
                temp = {
                    embed: {
                        url: data.url,
                        title: data.title,
                        color: 0x709A46,
                        image: {
                            url: data.image
                        },
                        author: data.source,
                        footer: {
                            text: data.author
                        }
                    }
                }
            })
            .catch((e) => {
                temp = {
                    embed: {
                        title: global.helpers.langs.get(this.lang, e.type),
                        color: 0xff0000,
                    }
                };
            });
        return temp;
    }

}

module.exports = ImagePost;
