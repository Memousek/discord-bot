const fetch = require('node-fetch');

class RedditImagePost{

    //ATTRIBUTES
    subreddit;

    //CONSTRUCTOR
    constructor(subreddit){
        this.subreddit = subreddit;
    }

    //METHODS
    async run() {
        var url = "https://www.reddit.com/r/" + this.subreddit + "/random.json?limit=1";
        const response = fetch(url);
        var toReturn;

        await response
            .then(res => res.json())
            .then(json => {
                var post = json[0].data.children[0].data;

                //IS IT VALID QUERY AND NOT A TEXT?
                if(json.length == null || post.is_self){
                    throw {type:"loading.couldnt-load-post"};
                    return;
                }

                //FORMATTED DATA
                var data = this.getImageData(post);

                //SETTING DATA FOR RETURN
                toReturn = {
                    url:data.domain+data.url,
                    title: data.title,
                    image:data.img,
                    source: {
                        name: data.subreddit,
                        url: data.domain+"/"+data.subreddit,
                    },
                    author: "u/"+ data.author
                };
            })
            .catch(err => {throw {type: "loading.couldnt-load-data"}});
        return toReturn;
    }
    getImageData(post){
        var img;
        var stupid = false;
        if(post.preview.reddit_video_preview != null && post.preview.reddit_video_preview.is_gif == true){ //GIF
            if(post.preview.images[0].variants == {} || post.domain == "i.imgur.com"){
                stupid = true;
                img = post.url;
            }else{
                img = post.preview.images[0].variants.gif.source.url.replace(/amps;s/g,"s");
            }
        }else if(post.post_hint == "image"){ //OBRÁZEK
            img = post.url;
        }
        img = img.replace(/amp;s/g, "s");

        return {
            domain: "https://reddit.com",
            author: post.author,
            subreddit: post.subreddit_name_prefixed,
            img:img,
            title: post.title.substring(0,252)+((post.title.length >= 252) ? "..." : ""),
            url: post.permalink,
            without_embed: stupid
        };
    } //CUSTOM METHOD

}

module.exports = RedditImagePost;
