const Playing = require("discord-playing");

class RichPresence{
    static setPresence(bot,code){
        Playing(bot,{
            [code]:{
                live: "In-Game",
                games: {
                    "Star Citizen": true,

                }
            }
        });
    }
}

module.exports = RichPresence;