const localisation = require('../../config/localisation');

class Langs {
    static get(lang,key){
        if(localisation[lang] != null){
            if(localisation[lang][key] != null){
                return localisation[lang][key];
            }else{
                var err = "Langs.js - couldn't load key '"+lang+":"+key+"'";
            }
        }else{
            var err = "Langs.js - couldn't load language '"+lang+"'.";
        }
        console.log(err);
        return "";
    }
}

module.exports = Langs;
