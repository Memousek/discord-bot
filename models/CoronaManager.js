const Discord = require('discord.js');
const fs = require('fs');
const fetch = require('node-fetch');
const cheerio = require('cheerio');

const channels = require("../config/channels.json");

class CoronaManager{
    url = "https://coronavirus-tracker-api.herokuapp.com/v2/locations?source=jhu";

    loadedCountries = {};

    setChannel(g_uuid, ch_uuid){
        channels[g_uuid] = ch_uuid;
        this.save();
    }
    getChannel(g_uuid){
        return channels[g_uuid];
        this.save();
    }
    unsetChannel(g_uuid){
        channels[g_uuid] = null;
        this.save();
    }
    save(){
        fs.writeFile("../config/channels.json",channels,(err)=>{
            if(err) throw err;
            console.log("Channels saved");
        });
    }
    async getCountryData(countryCode){
        if(this.loadedCountries[countryCode]){
            return this.loadedCountries[countryCode];
        }
        var data = await this.setCountryData(countryCode)

        return data;
    }
    async setCountryDataInside(code){
        var seznam = fs.readFileSync('config/seznam.txt','utf8')

        const $ = cheerio.load(seznam);
        var res = $(".d-flex.flex-column.align-items-center.justify-content-end.mol-covid-stats__item").text();

    }
    async setCountryDataOutside(code){
        var response = await fetch(this.url+`&country_code=${code}`)
            .then(res => res.json())
            .then(json => {
                this.loadedCountries[code] = {
                    last_updated: new Date(json.locations[0].last_updated).getTime(),
                    name:json.locations[0].country,
                    population: json.locations[0].country_population,
                    confirmed: json.latest.confirmed,
                    deaths: json.latest.deaths,
                    recovered: json.latest.recovered
                }
            })
            .catch(e => {
                throw e;
            });
        return this.loadedCountries[code];
    }

}

module.exports = CoronaManager;
